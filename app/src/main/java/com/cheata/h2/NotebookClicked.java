package com.cheata.h2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class NotebookClicked extends AppCompatActivity {
    GridLayout gridLayout;
    LinearLayout btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook_clicked);

        gridLayout = findViewById(R.id.gr_note_clicked);
        btnAdd = findViewById(R.id.btn_add);
        //when click button
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NotebookClicked.this,Note.class);
                startActivityForResult(intent,111);
            }
        });
    }
//    for calculate dp
    private int dpSize(float dp){
        return Math.round(dp*this.getResources().getDisplayMetrics().density);
    }

    //create Layout
    private void createLayout(String title, String content)
    {
        LinearLayout linearLayout = new LinearLayout(this);
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.width = dpSize(190);
        params.height = dpSize(190);
        params.setMargins(20,20,0,0);
        linearLayout.setOrientation(linearLayout.VERTICAL);
        linearLayout.setBackground(getResources().getDrawable(R.color.colorGreen));
        linearLayout.setLayoutParams(params);

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setLines(1);
        textView.setTextColor(getResources().getColor(R.color.colorWhite));
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setPadding(10,10,0,0);
        textView.setText(title);
        linearLayout.addView(textView);

        TextView textView1 = new TextView(this);
        textView1.setTextSize(25);
//        set ... when text is so long
        textView1.setLines(2);
        textView1.setTextColor(getResources().getColor(R.color.colorWhite));
        textView1.setEllipsize(TextUtils.TruncateAt.END);
        textView1.setPadding(10,10,0,0);
        textView1.setText(content);
        linearLayout.addView(textView1);

        gridLayout.addView(linearLayout);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check that it is the SecondActivity with an OK result
        if (requestCode ==111) {
            if (resultCode == RESULT_OK) {
                gridLayout = findViewById(R.id.gr_note_clicked);
                btnAdd = findViewById(R.id.btn_add);
                GridLayout.LayoutParams params = new GridLayout.LayoutParams();
                // Get String data from Intent
                createLayout(data.getStringExtra("title"),data.getStringExtra("content"));
                if(gridLayout.getChildCount()>1){
                    params.width = dpSize(190);
                    params.height = dpSize(190);
                    params.setMargins(20,20,0,0);
                    btnAdd.setLayoutParams(params);
                }else{
                    params.width = GridLayout.LayoutParams.MATCH_PARENT;
                    btnAdd.setLayoutParams(params);
                }
                String suc = "successful";
                Toast.makeText(NotebookClicked.this,suc,Toast.LENGTH_LONG).show();
            }
        }}
}

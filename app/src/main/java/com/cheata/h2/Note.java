package com.cheata.h2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Note extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Button btnSave = findViewById(R.id.btn_save);
        Button btnCancel = findViewById(R.id.btn_cancel);
        final EditText txtTitle = findViewById(R.id.txt_title);
        final EditText txtContent = findViewById(R.id.txt_content);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Note.this,NotebookClicked.class);
                String title = txtTitle.getText().toString();
                String content = txtContent.getText().toString();
                intent.putExtra("title",title);
                intent.putExtra("content",content);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
    }
}
